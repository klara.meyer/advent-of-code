mod common;

#[derive(Debug, PartialEq, Eq)]
enum Instruction {
    NoOp,
    AddX(i64),
}

impl std::str::FromStr for Instruction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let instr = s.trim();
        if instr == "noop" {
            Ok(Instruction::NoOp)
        } else if instr.starts_with("addx ") {
            let (_, val_str) = instr.split_once(' ').unwrap();
            let val = val_str
                .parse::<i64>()
                .map_err(|err| format!("Error parsing value '{val_str}': {err}"))?;
            Ok(Instruction::AddX(val))
        } else {
            Err(format!("Unknown instruction '{s}'"))
        }
    }
}

struct ProgramState {
    cycle: i64,
    x: i64,
}

impl ProgramState {
    fn new() -> Self {
        Self { cycle: 0, x: 1 }
    }

    fn apply<O: ProgramObserver>(&mut self, instruction: &Instruction, observer: &mut O) {
        match instruction {
            Instruction::NoOp => {
                self.cycle += 1;
                observer.observe(self);
            }
            Instruction::AddX(val) => {
                self.cycle += 1;
                observer.observe(self);
                self.cycle += 1;
                observer.observe(self);
                self.x += val;
            }
        }
    }
    fn run<O: ProgramObserver>(&mut self, instructions: &[Instruction], observer: &mut O) {
        for instruction in instructions {
            self.apply(instruction, observer);
        }
    }
}

trait ProgramObserver {
    type Output;

    fn observe(&mut self, state: &ProgramState);

    fn result(&self) -> Self::Output;
}

struct SignalStrengthObserver {
    strength: i64,
}

impl SignalStrengthObserver {
    fn new() -> Self {
        Self { strength: 0 }
    }
}

impl ProgramObserver for SignalStrengthObserver {
    type Output = i64;

    fn observe(&mut self, state: &ProgramState) {
        if (state.cycle - 20) % 40 == 0 {
            self.strength += state.cycle * state.x;
        }
    }

    fn result(&self) -> i64 {
        self.strength
    }
}

struct CrtObserver {
    screen: String,
}

impl CrtObserver {
    fn new() -> Self {
        Self {
            screen: String::new(),
        }
    }
}

impl ProgramObserver for CrtObserver {
    type Output = String;

    fn observe(&mut self, state: &ProgramState) {
        let pos = (state.cycle - 1) % 40;
        let sprite_visible = (pos - state.x).abs() <= 1;
        let char = if sprite_visible { '#' } else { '.' };
        self.screen.push(char);
        if state.cycle % 40 == 0 {
            self.screen.push('\n');
        }
    }

    fn result(&self) -> String {
        self.screen.clone()
    }
}

fn simulate<O: ProgramObserver>(instructions: &[Instruction], mut observer: O) -> O::Output {
    ProgramState::new().run(instructions, &mut observer);
    observer.result()
}

fn part1(instructions: &[Instruction]) -> i64 {
    simulate(instructions, SignalStrengthObserver::new())
}

fn part2(instructions: &[Instruction]) -> String {
    simulate(instructions, CrtObserver::new())
}

fn main() {
    let input = common::get_input::<Instruction>();
    println!("Part1: {}", part1(&input));
    println!("Part2:\n{}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_input() -> Vec<Instruction> {
        "noop\n\
        addx 3\n\
        addx -5"
            .lines()
            .map(|line| line.parse::<Instruction>().unwrap())
            .collect()
    }

    fn test_input2() -> Vec<Instruction> {
        "addx 15\n\
        addx -11\n\
        addx 6\n\
        addx -3\n\
        addx 5\n\
        addx -1\n\
        addx -8\n\
        addx 13\n\
        addx 4\n\
        noop\n\
        addx -1\n\
        addx 5\n\
        addx -1\n\
        addx 5\n\
        addx -1\n\
        addx 5\n\
        addx -1\n\
        addx 5\n\
        addx -1\n\
        addx -35\n\
        addx 1\n\
        addx 24\n\
        addx -19\n\
        addx 1\n\
        addx 16\n\
        addx -11\n\
        noop\n\
        noop\n\
        addx 21\n\
        addx -15\n\
        noop\n\
        noop\n\
        addx -3\n\
        addx 9\n\
        addx 1\n\
        addx -3\n\
        addx 8\n\
        addx 1\n\
        addx 5\n\
        noop\n\
        noop\n\
        noop\n\
        noop\n\
        noop\n\
        addx -36\n\
        noop\n\
        addx 1\n\
        addx 7\n\
        noop\n\
        noop\n\
        noop\n\
        addx 2\n\
        addx 6\n\
        noop\n\
        noop\n\
        noop\n\
        noop\n\
        noop\n\
        addx 1\n\
        noop\n\
        noop\n\
        addx 7\n\
        addx 1\n\
        noop\n\
        addx -13\n\
        addx 13\n\
        addx 7\n\
        noop\n\
        addx 1\n\
        addx -33\n\
        noop\n\
        noop\n\
        noop\n\
        addx 2\n\
        noop\n\
        noop\n\
        noop\n\
        addx 8\n\
        noop\n\
        addx -1\n\
        addx 2\n\
        addx 1\n\
        noop\n\
        addx 17\n\
        addx -9\n\
        addx 1\n\
        addx 1\n\
        addx -3\n\
        addx 11\n\
        noop\n\
        noop\n\
        addx 1\n\
        noop\n\
        addx 1\n\
        noop\n\
        noop\n\
        addx -13\n\
        addx -19\n\
        addx 1\n\
        addx 3\n\
        addx 26\n\
        addx -30\n\
        addx 12\n\
        addx -1\n\
        addx 3\n\
        addx 1\n\
        noop\n\
        noop\n\
        noop\n\
        addx -9\n\
        addx 18\n\
        addx 1\n\
        addx 2\n\
        noop\n\
        noop\n\
        addx 9\n\
        noop\n\
        noop\n\
        noop\n\
        addx -1\n\
        addx 2\n\
        addx -37\n\
        addx 1\n\
        addx 3\n\
        noop\n\
        addx 15\n\
        addx -21\n\
        addx 22\n\
        addx -6\n\
        addx 1\n\
        noop\n\
        addx 2\n\
        addx 1\n\
        noop\n\
        addx -10\n\
        noop\n\
        noop\n\
        addx 20\n\
        addx 1\n\
        addx 2\n\
        addx 2\n\
        addx -6\n\
        addx -11\n\
        noop\n\
        noop\n\
        noop"
            .lines()
            .map(|line| line.parse::<Instruction>().unwrap())
            .collect()
    }

    #[test]
    fn test_parsing() {
        let input = test_input();
        use Instruction::*;
        assert_eq!(input, vec![NoOp, AddX(3), AddX(-5)]);
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(&test_input2()), 13140);
    }

    #[test]
    fn test_part2() {
        assert_eq!(
            part2(&test_input2()),
            "##..##..##..##..##..##..##..##..##..##..\n\
            ###...###...###...###...###...###...###.\n\
            ####....####....####....####....####....\n\
            #####.....#####.....#####.....#####.....\n\
            ######......######......######......####\n\
            #######.......#######.......#######.....\n"
                .to_string()
        );
    }
}
