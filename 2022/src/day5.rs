mod common;

#[derive(Debug, PartialEq, Eq, Clone)]
struct Crates {
    stacks: Vec<Vec<char>>,
}

#[derive(Debug, PartialEq, Eq)]
struct Move {
    num: usize,
    from: usize,
    to: usize,
}

impl Move {
    fn parse(input: &str) -> Self {
        let mut split = input.split_ascii_whitespace();
        let _move_str = split.next().unwrap();
        let num = split.next().unwrap().parse::<usize>().unwrap();
        let _from_str = split.next().unwrap();
        let from = split.next().unwrap().parse::<usize>().unwrap();
        let _to_str = split.next().unwrap();
        let to = split.next().unwrap().parse::<usize>().unwrap();
        Move { num, from, to }
    }
}

impl Crates {
    fn move_9000(&mut self, m: &Move) {
        for _ in 0..m.num {
            let item = self.stacks[m.from - 1].pop().unwrap();
            self.stacks[m.to - 1].push(item);
        }
    }

    fn move_9001(&mut self, m: &Move) {
        let height = self.stacks[m.from - 1].len();
        let items: Vec<_> = self.stacks[m.from - 1].drain((height - m.num)..).collect();
        self.stacks[m.to - 1].extend(items);
    }

    fn message(&self) -> String {
        self.stacks
            .iter()
            .filter_map(|stack| stack.last())
            .collect()
    }
}

fn part1(mut crates: Crates, moves: &[Move]) -> String {
    for m in moves {
        crates.move_9000(m);
    }
    crates.message()
}

fn part2(mut crates: Crates, moves: &[Move]) -> String {
    for m in moves {
        crates.move_9001(m);
    }
    crates.message()
}

fn parse_crates(lines: &[&str]) -> Crates {
    let (num_line, crate_lines) = lines.split_last().unwrap();
    let num_stacks = num_line
        .split_ascii_whitespace()
        .last()
        .unwrap()
        .parse::<usize>()
        .unwrap();
    let mut stacks: Vec<Vec<char>> = vec![vec![]; num_stacks];
    for crate_line in crate_lines {
        let mut first;
        let mut rest = *crate_line;
        for stack in &mut stacks {
            (first, rest) = rest.split_at(std::cmp::min(4, rest.len()));
            first = first.trim_matches(|c| matches!(c, ' ' | '[' | ']'));
            if let Some(item) = first.chars().next() {
                stack.push(item);
            }
            if rest.is_empty() {
                break;
            }
        }
    }
    for stack in &mut stacks {
        stack.reverse();
    }
    Crates { stacks }
}

fn parse_moves(lines: &[&str]) -> Vec<Move> {
    lines.iter().map(|line| Move::parse(line)).collect()
}

fn main() {
    let input = common::get_lines();
    let input_ref: Vec<_> = input.iter().map(String::as_str).collect();
    let split_line = input_ref.iter().position(|line| line.is_empty()).unwrap();
    let (crate_lines, move_lines) = input_ref.split_at(split_line);
    let move_lines = &move_lines[1..];
    let crates = parse_crates(crate_lines);
    let moves = parse_moves(move_lines);
    println!("Part1: {}", part1(crates.clone(), &moves));
    println!("Part2: {}", part2(crates, &moves));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_crates() -> Crates {
        let input = "    [D]    \n\
                     [N] [C]    \n\
                     [Z] [M] [P]\n\
                     1   2   3 ";
        parse_crates(&input.lines().collect::<Vec<_>>())
    }

    fn test_moves() -> Vec<Move> {
        let input = "move 1 from 2 to 1\n\
                     move 3 from 1 to 3\n\
                     move 2 from 2 to 1\n\
                     move 1 from 1 to 2";
        parse_moves(&input.lines().collect::<Vec<_>>())
    }

    #[test]
    fn test_parse_crates() {
        assert_eq!(
            test_crates(),
            Crates {
                stacks: vec![vec!['Z', 'N'], vec!['M', 'C', 'D'], vec!['P']]
            }
        )
    }

    #[test]
    fn test_parse_moves() {
        assert_eq!(
            test_moves(),
            vec![
                Move {
                    num: 1,
                    from: 2,
                    to: 1
                },
                Move {
                    num: 3,
                    from: 1,
                    to: 3
                },
                Move {
                    num: 2,
                    from: 2,
                    to: 1
                },
                Move {
                    num: 1,
                    from: 1,
                    to: 2
                },
            ]
        )
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(test_crates(), &test_moves()), "CMZ".to_string());
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(test_crates(), &test_moves()), "MCD".to_string());
    }
}
