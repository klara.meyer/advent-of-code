mod common;

fn part1(input: &[Vec<i32>]) -> i32 {
    input
        .iter()
        .map(|calories| calories.iter().sum())
        .max()
        .unwrap()
}

fn part2(input: &[Vec<i32>]) -> i32 {
    // keep current three maximal values in sorted order
    // algorithm runs in linear time since sorting just three elements is a constant
    let mut max_three = [0; 3];
    for calories in input.iter().map(|calories| calories.iter().sum()) {
        max_three[0] = std::cmp::max(max_three[0], calories);
        max_three.sort_unstable();
    }
    max_three.iter().sum()
}

fn main() {
    let input: Vec<Vec<i32>> = common::get_input_grouped();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(
            part1(&[
                vec![1000, 2000, 3000],
                vec![4000],
                vec![5000, 6000],
                vec![7000, 8000, 9000],
                vec![10000]
            ]),
            24000
        );
    }

    #[test]
    fn test_part2() {
        assert_eq!(
            part2(&[
                vec![1000, 2000, 3000],
                vec![4000],
                vec![5000, 6000],
                vec![7000, 8000, 9000],
                vec![10000]
            ]),
            45000
        );
    }
}
