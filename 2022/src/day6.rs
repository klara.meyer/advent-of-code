mod common;

use std::collections::HashSet;

fn is_marker(data: &[u8]) -> bool {
    data.iter().collect::<HashSet<_>>().len() == data.len()
}

fn find_marker<const N: usize>(data: &str) -> usize {
    // note: on nightly we could use `array_windows` and use `N` also in `is_marker`
    data.as_bytes().windows(N).position(is_marker).unwrap() + N
}

fn part1(data: &str) -> usize {
    find_marker::<4>(data)
}

fn part2(data: &str) -> usize {
    find_marker::<14>(data)
}

fn main() {
    let input = common::get_content();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 7);
        assert_eq!(part1("bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(part1("nppdvjthqldpwncqszvftbrmjlhg"), 6);
        assert_eq!(part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 10);
        assert_eq!(part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 19);
        assert_eq!(part2("bvwbjplbgvbhsrlpgdmjqwftvncz"), 23);
        assert_eq!(part2("nppdvjthqldpwncqszvftbrmjlhg"), 23);
        assert_eq!(part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 29);
        assert_eq!(part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 26);
    }
}
