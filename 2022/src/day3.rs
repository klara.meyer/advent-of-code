mod common;

use std::collections::HashSet;

fn common_item(elements: &[&str]) -> char {
    let (first, rest) = elements.split_first().unwrap();
    let rest_sets: Vec<HashSet<char>> = rest.iter().map(|r| r.chars().collect()).collect();
    first
        .chars()
        .find(|c| rest_sets.iter().all(|r| r.contains(c)))
        .unwrap()
}

fn priority(item: char) -> i32 {
    if ('a'..='z').contains(&item) {
        (item as i32) - ('a' as i32) + 1
    } else {
        (item as i32) - ('A' as i32) + 27
    }
}

fn part1(input: &[&str]) -> i32 {
    input
        .iter()
        .map(|backpack| {
            priority(common_item(&[
                &backpack[..(backpack.len() / 2)],
                &backpack[(backpack.len() / 2)..],
            ]))
        })
        .sum()
}

fn part2(input: &[&str]) -> i32 {
    input
        .chunks_exact(3)
        .into_iter()
        .map(|group| priority(common_item(group)))
        .sum()
}

fn main() {
    let input = common::get_lines();
    let input_ref: Vec<_> = input.iter().map(String::as_str).collect();
    println!("Part1: {}", part1(&input_ref));
    println!("Part2: {}", part2(&input_ref));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(
            part1(&[
                "vJrwpWtwJgWrhcsFMMfFFhFp",
                "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                "PmmdzqPrVvPwwTWBwg",
                "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                "ttgJtRGJQctTZtZT",
                "CrZsJsPPZsGzwwsLwLmpwMDw"
            ]),
            157
        );
    }

    #[test]
    fn test_part2() {
        assert_eq!(
            part2(&[
                "vJrwpWtwJgWrhcsFMMfFFhFp",
                "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                "PmmdzqPrVvPwwTWBwg",
                "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                "ttgJtRGJQctTZtZT",
                "CrZsJsPPZsGzwwsLwLmpwMDw"
            ]),
            70
        );
    }
}
