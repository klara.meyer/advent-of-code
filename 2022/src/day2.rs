mod common;

fn score(other: char, own: char) -> i32 {
    let win = match (other, own) {
        ('A', 'X') => 3,
        ('A', 'Y') => 6,
        ('A', 'Z') => 0,
        ('B', 'X') => 0,
        ('B', 'Y') => 3,
        ('B', 'Z') => 6,
        ('C', 'X') => 6,
        ('C', 'Y') => 0,
        ('C', 'Z') => 3,
        _ => panic!("unsupported shape"),
    };
    let shape = match own {
        'X' => 1,
        'Y' => 2,
        'Z' => 3,
        _ => panic!("unsupported shape"),
    };
    win + shape
}

fn part1(input: &[(char, char)]) -> i32 {
    input.iter().map(|&(other, own)| score(other, own)).sum()
}

fn score_for_outcome(other: char, outcome: char) -> i32 {
    let own = match (other, outcome) {
        ('A', 'X') => 'Z',
        ('A', 'Y') => 'X',
        ('A', 'Z') => 'Y',
        ('B', 'X') => 'X',
        ('B', 'Y') => 'Y',
        ('B', 'Z') => 'Z',
        ('C', 'X') => 'Y',
        ('C', 'Y') => 'Z',
        ('C', 'Z') => 'X',
        _ => panic!("unsupported shape"),
    };
    score(other, own)
}

fn part2(input: &[(char, char)]) -> i32 {
    input
        .iter()
        .map(|&(other, own)| score_for_outcome(other, own))
        .sum()
}

fn main() {
    let input: Vec<(char, char)> = common::get_lines()
        .into_iter()
        .map(|line| {
            let mut split = line.split_ascii_whitespace();
            let c1 = split.next().unwrap().chars().next().unwrap();
            let c2 = split.next().unwrap().chars().next().unwrap();
            (c1, c2)
        })
        .collect();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(&[('A', 'Y'), ('B', 'X'), ('C', 'Z'),]), 15);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(&[('A', 'Y'), ('B', 'X'), ('C', 'Z'),]), 12);
    }
}
