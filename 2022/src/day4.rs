mod common;

#[derive(Debug)]
struct Section {
    begin: u32,
    end: u32,
}

impl Section {
    fn parse(section: &str) -> Self {
        let (begin, end) = section.split_once('-').unwrap();
        let begin = begin.parse::<u32>().unwrap();
        let end = end.parse::<u32>().unwrap();
        assert!(begin <= end);
        Section { begin, end }
    }

    fn as_range(&self) -> std::ops::RangeInclusive<u32> {
        self.begin..=self.end
    }

    fn fully_contains(&self, other: &Section) -> bool {
        self.as_range().contains(&other.begin) && self.as_range().contains(&other.end)
    }

    fn overlaps_with(&self, other: &Section) -> bool {
        self.as_range().contains(&other.begin)
            || self.as_range().contains(&other.end)
            || other.fully_contains(self)
    }
}

fn part1(input: &[(Section, Section)]) -> usize {
    input
        .iter()
        .filter(|(first, second)| first.fully_contains(second) || second.fully_contains(first))
        .count()
}

fn part2(input: &[(Section, Section)]) -> usize {
    input
        .iter()
        .filter(|(first, second)| first.overlaps_with(second))
        .count()
}

fn parse_line(line: &str) -> (Section, Section) {
    let (first, second) = line.split_once(',').unwrap();
    let first = Section::parse(first);
    let second = Section::parse(second);
    (first, second)
}

fn main() {
    let input: Vec<_> = common::get_lines()
        .into_iter()
        .map(|line| parse_line(&line))
        .collect();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_input() -> Vec<(Section, Section)> {
        vec![
            parse_line("2-4,6-8"),
            parse_line("2-3,4-5"),
            parse_line("5-7,7-9"),
            parse_line("2-8,3-7"),
            parse_line("6-6,4-6"),
            parse_line("2-6,4-8"),
        ]
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(&test_input()), 2);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(&test_input()), 4);
    }
}
