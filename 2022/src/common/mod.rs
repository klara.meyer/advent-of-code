use std::env;
use std::fs;
use std::io::BufRead;
use std::str::FromStr;

use itertools::Itertools;

fn get_filename() -> String {
    env::args().nth(1).expect("no filename given")
}

#[allow(dead_code)]
pub fn get_content() -> String {
    let filename = get_filename();
    fs::read_to_string(filename).expect("could not read file")
}

#[allow(dead_code)]
pub fn get_lines() -> Vec<String> {
    let filename = get_filename();
    let file = std::fs::File::open(filename).expect("could not open file");
    let buffer = std::io::BufReader::new(file);

    buffer
        .lines()
        .map(|l| l.expect("could not read line"))
        .collect()
}

#[allow(dead_code)]
pub fn get_input<T: FromStr>() -> Vec<T>
where
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    get_lines()
        .into_iter()
        .map(|l| l.parse::<T>().expect("could not parse input"))
        .collect()
}

#[allow(dead_code)]
pub fn get_input_grouped<T: FromStr>() -> Vec<Vec<T>>
where
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    get_lines()
        .into_iter()
        .group_by(|l| l.is_empty())
        .into_iter()
        .filter_map(|(is_empty, group)| {
            (!is_empty).then(|| {
                group
                    .into_iter()
                    .map(|line| line.parse::<T>().expect("could not parse input"))
                    .collect()
            })
        })
        .collect()
}
