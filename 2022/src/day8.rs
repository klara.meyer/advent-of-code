mod common;

use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Tree(u32);

impl Tree {
    fn parse(input: char) -> Tree {
        Tree(input.to_digit(10).unwrap())
    }

    fn height(&self) -> u32 {
        self.0
    }
}

impl std::fmt::Display for Tree {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Position {
    x: usize,
    y: usize,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct TreeWithPosition {
    position: Position,
    tree: Tree,
}

#[derive(Debug, PartialEq, Eq)]
struct Grid {
    grid: Vec<Vec<Tree>>,
    width: usize,
    height: usize,
}

struct GridWalker<'a> {
    grid: &'a Grid,
    x: usize,
    y: usize,
    x_delta: isize,
    y_delta: isize,
}

impl<'a> Iterator for GridWalker<'a> {
    type Item = TreeWithPosition;

    fn next(&mut self) -> Option<Self::Item> {
        if self.x >= self.grid.width || self.y >= self.grid.height {
            None
        } else {
            let tree = self.grid.grid[self.y][self.x];
            let item = TreeWithPosition {
                position: Position {
                    x: self.x,
                    y: self.y,
                },
                tree,
            };
            self.x = (self.x as isize + self.x_delta) as usize;
            self.y = (self.y as isize + self.y_delta) as usize;
            Some(item)
        }
    }
}

struct VisibleTreeObserver<I> {
    iter: I,
    highest: Option<Tree>,
}

impl<I: Iterator<Item = TreeWithPosition>> VisibleTreeObserver<I> {
    fn new(iter: I) -> Self {
        Self {
            iter,
            highest: None,
        }
    }
}

impl<I: Iterator<Item = TreeWithPosition>> Iterator for VisibleTreeObserver<I> {
    type Item = TreeWithPosition;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().and_then(|tree_pos| {
            let higher = self
                .highest
                .map_or(true, |highest| highest.height() < tree_pos.tree.height());
            self.highest = higher.then_some(tree_pos.tree);
            if higher {
                Some(tree_pos)
            } else {
                // look for remaining higher trees
                self.next()
            }
        })
    }
}

struct ScenicTreeObserver<I> {
    iter: I,
    start: Tree,
    done: bool,
}

impl<I: Iterator<Item = TreeWithPosition>> ScenicTreeObserver<I> {
    fn new(mut iter: I) -> Self {
        let start = iter.next().unwrap().tree;
        Self {
            iter,
            start,
            done: false,
        }
    }
}

impl<I: Iterator<Item = TreeWithPosition>> Iterator for ScenicTreeObserver<I> {
    type Item = TreeWithPosition;

    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }
        self.iter.next().map(|tree_pos| {
            self.done = self.start.height() <= tree_pos.tree.height();
            tree_pos
        })
    }
}

impl Grid {
    fn parse(input: &str) -> Grid {
        let grid: Vec<Vec<Tree>> = input
            .lines()
            .map(|line| line.chars().map(Tree::parse).collect())
            .collect();
        assert!(!grid.is_empty());
        let height = grid.len();
        let width = grid[0].len();
        assert!(grid.iter().all(|row| row.len() == width));
        Grid {
            grid,
            width,
            height,
        }
    }

    fn walk(
        &self,
        start: (usize, usize),
        dir: (isize, isize),
    ) -> impl Iterator<Item = TreeWithPosition> + '_ {
        GridWalker {
            grid: self,
            x: start.0,
            y: start.1,
            x_delta: dir.0,
            y_delta: dir.1,
        }
    }

    fn count_visible_trees(&self) -> usize {
        let mut trees = HashSet::<TreeWithPosition>::new();
        // left to right and right to left
        for y in 0..self.height {
            trees.extend(VisibleTreeObserver::new(self.walk((0, y), (1, 0))));
            trees.extend(VisibleTreeObserver::new(
                self.walk((self.width - 1, y), (-1, 0)),
            ));
        }
        // top to bottom and bottom to top
        for x in 0..self.width {
            trees.extend(VisibleTreeObserver::new(self.walk((x, 0), (0, 1))));
            trees.extend(VisibleTreeObserver::new(
                self.walk((x, self.height - 1), (0, -1)),
            ));
        }
        trees.len()
    }

    fn scenic_score(&self, pos: (usize, usize)) -> usize {
        let left = ScenicTreeObserver::new(self.walk(pos, (-1, 0))).count();
        let right = ScenicTreeObserver::new(self.walk(pos, (1, 0))).count();
        let up = ScenicTreeObserver::new(self.walk(pos, (0, -1))).count();
        let down = ScenicTreeObserver::new(self.walk(pos, (0, 1))).count();
        left * right * up * down
    }

    fn highest_scenic_score(&self) -> usize {
        let mut highest_score = 0;
        for x in 0..self.width {
            for y in 0..self.height {
                let score = self.scenic_score((x, y));
                highest_score = std::cmp::max(score, highest_score);
            }
        }
        highest_score
    }
}

fn part1(grid: &Grid) -> usize {
    grid.count_visible_trees()
}

fn part2(grid: &Grid) -> usize {
    grid.highest_scenic_score()
}

fn main() {
    let input = Grid::parse(&common::get_content());
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_input() -> Grid {
        Grid::parse(
            "30373\n\
             25512\n\
             65332\n\
             33549\n\
             35390\n",
        )
    }

    #[test]
    fn test_parsing() {
        let input = test_input();
        assert_eq!(
            input.grid,
            vec![
                vec![Tree(3), Tree(0), Tree(3), Tree(7), Tree(3)],
                vec![Tree(2), Tree(5), Tree(5), Tree(1), Tree(2)],
                vec![Tree(6), Tree(5), Tree(3), Tree(3), Tree(2)],
                vec![Tree(3), Tree(3), Tree(5), Tree(4), Tree(9)],
                vec![Tree(3), Tree(5), Tree(3), Tree(9), Tree(0)]
            ]
        );
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(&test_input()), 21);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(&test_input()), 8);
    }
}
