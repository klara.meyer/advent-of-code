mod common;

use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Dir {
    L,
    R,
    U,
    D,
}

impl std::str::FromStr for Dir {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Dir::*;
        let dir = match s {
            "L" => L,
            "R" => R,
            "U" => U,
            "D" => D,
            _ => return Err(format!("unknown direction: {s}")),
        };
        Ok(dir)
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Move {
    dir: Dir,
    num: i32,
}

impl std::str::FromStr for Move {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (dir, num) = s.trim().split_once(' ').unwrap();
        let dir = dir.parse::<Dir>()?;
        let num = num
            .parse::<i32>()
            .map_err(|err| format!("Error parsing num: {err}"))?;
        Ok(Move { dir, num })
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
struct Position {
    x: i32,
    y: i32,
}

impl Position {
    fn new() -> Self {
        Position { x: 0, y: 0 }
    }

    fn apply_direction(&mut self, dir: Dir) {
        use Dir::*;
        match dir {
            L => self.x -= 1,
            R => self.x += 1,
            U => self.y -= 1,
            D => self.y += 1,
        }
    }

    fn follow(&mut self, other: &Position) {
        let x_delta = other.x - self.x;
        let y_delta = other.y - self.y;
        if y_delta.abs() > 1 || (y_delta.abs() > 0 && x_delta.abs() > 1) {
            self.y += y_delta.signum();
        }
        if x_delta.abs() > 1 || (x_delta.abs() > 0 && y_delta.abs() > 1) {
            self.x += x_delta.signum();
        }
    }
}

#[derive(Debug)]
struct Rope {
    knots: Vec<Position>,
    tail_history: HashSet<Position>,
}

impl Rope {
    fn new(size: usize) -> Self {
        assert!(size >= 1);
        let knots = vec![Position::new(); size];
        let mut tail_history = HashSet::new();
        tail_history.insert(*knots.last().unwrap());
        Rope {
            knots,
            tail_history,
        }
    }

    fn apply_move(&mut self, m: &Move) {
        for _ in 0..m.num {
            self.knots[0].apply_direction(m.dir);
            let mut previous = self.knots[0];
            for tail in &mut self.knots[1..] {
                tail.follow(&previous);
                previous = *tail;
            }
            self.tail_history.insert(*self.knots.last().unwrap());
        }
    }

    fn count_tail_positions_for(&mut self, moves: &[Move]) -> usize {
        for m in moves {
            self.apply_move(m);
        }
        self.tail_history.len()
    }
}

fn part1(moves: &[Move]) -> usize {
    Rope::new(2).count_tail_positions_for(moves)
}

fn part2(moves: &[Move]) -> usize {
    Rope::new(10).count_tail_positions_for(moves)
}

fn main() {
    let input = common::get_input::<Move>();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_input() -> Vec<Move> {
        "R 4\n\
        U 4\n\
        L 3\n\
        D 1\n\
        R 4\n\
        D 1\n\
        L 5\n\
        R 2"
        .lines()
        .map(|line| line.parse::<Move>().unwrap())
        .collect()
    }

    fn test_input2() -> Vec<Move> {
        "R 5\n\
        U 8\n\
        L 8\n\
        D 3\n\
        R 17\n\
        D 10\n\
        L 25\n\
        U 20"
            .lines()
            .map(|line| line.parse::<Move>().unwrap())
            .collect()
    }

    #[test]
    fn test_parsing() {
        let input = test_input();
        use Dir::*;
        assert_eq!(
            input,
            vec![
                Move { dir: R, num: 4 },
                Move { dir: U, num: 4 },
                Move { dir: L, num: 3 },
                Move { dir: D, num: 1 },
                Move { dir: R, num: 4 },
                Move { dir: D, num: 1 },
                Move { dir: L, num: 5 },
                Move { dir: R, num: 2 }
            ]
        );
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(&test_input()), 13);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(&test_input()), 1);
        assert_eq!(part2(&test_input2()), 36);
    }
}
