mod common;

use std::collections::HashMap;
use std::path::Component;
use std::path::Components;
use std::path::Path;
use std::path::PathBuf;

#[derive(Debug, Clone, PartialEq, Eq)]
enum HistoryLine {
    Command(Command),
    FileEntry(FileEntry),
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Command {
    Cd(PathBuf),
    Ls,
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum FileEntry {
    Dir { name: String },
    File { name: String, size: usize },
}

impl std::str::FromStr for HistoryLine {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("$ ") {
            let (_, cmd) = s.split_once(' ').unwrap();
            let cmd = cmd.parse::<Command>()?;
            Ok(HistoryLine::Command(cmd))
        } else {
            let file_entry = s.parse::<FileEntry>()?;
            Ok(HistoryLine::FileEntry(file_entry))
        }
    }
}

impl std::str::FromStr for Command {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("cd ") {
            let (_, path) = s.split_once(' ').unwrap();
            let path = path
                .parse::<PathBuf>()
                .map_err(|err| format!("Invalid path '{path}': {err}"))?;
            Ok(Command::Cd(path))
        } else if s.starts_with("ls") {
            Ok(Command::Ls)
        } else {
            Err(format!("Unknown command '{s}'"))
        }
    }
}

impl std::str::FromStr for FileEntry {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (info, name) = s
            .split_once(' ')
            .ok_or_else(|| format!("Invalid file enty '{s}'"))?;
        let name = name.to_string();
        if info == "dir" {
            Ok(FileEntry::Dir { name })
        } else {
            let size = info
                .parse::<usize>()
                .map_err(|err| format!("Invalid file size '{info}': {err}"))?;
            Ok(FileEntry::File { name, size })
        }
    }
}

pub fn normalize_path(path: &Path) -> PathBuf {
    let mut ret = PathBuf::new();
    for component in path.components() {
        match component {
            Component::Prefix(_) => panic!("unexpected prefix in path"),
            Component::RootDir => ret.push(component.as_os_str()),
            Component::CurDir => (),
            Component::ParentDir => assert!(ret.pop()),
            Component::Normal(c) => ret.push(c),
        }
    }
    ret
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Directory {
    directories: HashMap<String, Directory>,
    files: HashMap<String, usize>,
}

impl Directory {
    fn new() -> Self {
        Directory {
            directories: HashMap::new(),
            files: HashMap::new(),
        }
    }

    fn dir_sizes(&self) -> usize {
        self.directories.values().map(Self::total_size).sum()
    }

    fn file_sizes(&self) -> usize {
        self.files.values().sum()
    }

    fn total_size(&self) -> usize {
        self.dir_sizes() + self.file_sizes()
    }

    fn all_dir_sizes(&self) -> Vec<usize> {
        self.directories
            .values()
            .flat_map(Self::all_dir_sizes)
            .chain(std::iter::once(self.total_size()))
            .collect()
    }

    fn follow(&mut self, mut components: Components) -> &mut Self {
        match components.next() {
            Some(component) => match component {
                Component::Prefix(_) => panic!("unexpected prefix in path"),
                Component::RootDir => panic!("unexpected root in path"),
                Component::CurDir => panic!("unexpected cur dir path"),
                Component::ParentDir => panic!("unexpected parent dir in path"),
                Component::Normal(target) => {
                    let target_str = target.to_str().expect("target not valid unicode");
                    match self.directories.get_mut(target_str) {
                        Some(dir) => dir.follow(components),
                        None => panic!("did not find target dir '{target_str}'"),
                    }
                }
            },
            None => self,
        }
    }

    fn add_dir(&mut self, name: &str) {
        self.directories.insert(name.to_owned(), Directory::new());
    }

    fn add_file(&mut self, name: &str, size: usize) {
        self.files.insert(name.to_owned(), size);
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Shell {
    root: Directory,
    current_dir: PathBuf,
}

impl Shell {
    fn new() -> Self {
        Shell {
            root: Directory::new(),
            current_dir: PathBuf::new(),
        }
    }

    fn from_history(history: &[HistoryLine]) -> Self {
        let mut shell = Shell::new();
        for line in history {
            shell.apply(line);
        }
        shell
    }

    fn current_dir(&mut self) -> &mut Directory {
        let path = self.current_dir.clone();
        let dir = &mut self.root;
        let mut components = path.components();
        assert_eq!(
            components.next(),
            Some(Component::RootDir),
            "path should start with root dir"
        );
        dir.follow(components)
    }

    fn apply(&mut self, line: &HistoryLine) {
        match line {
            HistoryLine::Command(cmd) => match cmd {
                Command::Cd(path) => {
                    self.current_dir.push(path);
                    self.current_dir = normalize_path(&self.current_dir);
                }
                Command::Ls => (), // can skip this
            },
            HistoryLine::FileEntry(entry) => {
                let current_dir = self.current_dir();
                match entry {
                    FileEntry::Dir { name } => current_dir.add_dir(name),
                    FileEntry::File { name, size } => current_dir.add_file(name, *size),
                }
            }
        }
    }

    fn total_size(&self) -> usize {
        self.root.total_size()
    }

    fn all_dir_sizes(&self) -> Vec<usize> {
        self.root.all_dir_sizes()
    }
}

fn part1(history: &[HistoryLine]) -> usize {
    const SMALL_DIR_SIZE: usize = 100_000;
    Shell::from_history(history)
        .all_dir_sizes()
        .iter()
        .filter(|&&size| size <= SMALL_DIR_SIZE)
        .sum()
}

fn part2(history: &[HistoryLine]) -> usize {
    const TOTAL_SPACE: usize = 70_000_000;
    const REQUIRED_FREE_SPACE: usize = 30_000_000;
    let shell = Shell::from_history(history);
    let total = shell.total_size();
    assert!(total <= TOTAL_SPACE);
    *shell
        .all_dir_sizes()
        .iter()
        .filter(|&&size|  total - size <= TOTAL_SPACE - REQUIRED_FREE_SPACE)
        .min()
        .unwrap()
}

fn main() {
    let input = common::get_input::<HistoryLine>();
    println!("Part1: {}", part1(&input));
    println!("Part2: {}", part2(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_input() -> Vec<HistoryLine> {
        "$ cd /\n\
        $ ls\n\
        dir a\n\
        14848514 b.txt\n\
        8504156 c.dat\n\
        dir d\n\
        $ cd a\n\
        $ ls\n\
        dir e\n\
        29116 f\n\
        2557 g\n\
        62596 h.lst\n\
        $ cd e\n\
        $ ls\n\
        584 i\n\
        $ cd ..\n\
        $ cd ..\n\
        $ cd d\n\
        $ ls\n\
        4060174 j\n\
        8033020 d.log\n\
        5626152 d.ext\n\
        7214296 k"
            .lines()
            .map(|line| line.parse::<HistoryLine>().unwrap())
            .collect()
    }

    #[test]
    fn test_parsing() {
        let input = test_input();
        assert_eq!(
            input[0],
            HistoryLine::Command(Command::Cd("/".parse().unwrap()))
        );
        assert_eq!(input[1], HistoryLine::Command(Command::Ls));
        assert_eq!(
            input[2],
            HistoryLine::FileEntry(FileEntry::Dir {
                name: "a".to_string()
            })
        );
        assert_eq!(
            input[3],
            HistoryLine::FileEntry(FileEntry::File {
                name: "b.txt".to_string(),
                size: 14848514
            })
        );
        assert_eq!(
            input[6],
            HistoryLine::Command(Command::Cd("a".parse().unwrap()))
        );
        assert_eq!(
            input[15],
            HistoryLine::Command(Command::Cd("..".parse().unwrap()))
        );
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(&test_input()), 95437);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(&test_input()), 24933642);
    }
}
